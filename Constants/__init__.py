from bearlibterminal import terminal

from Components.Fighter import Fighter
from Components.Inventory import Inventory
from Components.Level import Level
from Entity import Entity
from Game import MessageLog, States
from MapObjects import GameMap
from Render import RenderOrder

window_title = 'CyberRogue'

screen_width = 80
screen_height = 50

bar_width = 20
panel_height = 7
panel_y = screen_height - panel_height

message_x = bar_width + 2
message_width = screen_width - bar_width - 2
message_height = panel_height - 1

map_width = 80
map_height = 43

room_max_size = 10
room_min_size = 6
max_rooms = 30

max_monsters_per_room = 3
max_items_per_room = 2

fov_algorithm = 0
fov_light_walls = True
fov_radius = 10

colors = {
    'dark_wall': terminal.color_from_argb(255, 0, 0, 100),
    'dark_ground': terminal.color_from_argb(255, 50, 50, 150),
    'light_wall': terminal.color_from_argb(255, 130, 110, 50),
    'light_ground': terminal.color_from_argb(255, 200, 180, 50),
    'white': terminal.color_from_argb(255, 255, 255, 255),
    'desaturated_green': terminal.color_from_argb(255, 64, 128, 64),
    'darker_green': terminal.color_from_argb(255, 0, 128, 0),
    'dark_red': terminal.color_from_argb(255, 191, 0, 0),
    'light_red': terminal.color_from_argb(255, 255, 115, 115),
    'darker_red': terminal.color_from_argb(255, 128, 0, 0),
    'violet': terminal.color_from_argb(255, 127, 0, 255),
    'yellow': terminal.color_from_argb(255, 255, 255, 0),
    'blue': terminal.color_from_argb(255, 0, 0, 255),
    'green': terminal.color_from_argb(255, 0, 255, 0),
    'red': terminal.color_from_argb(255, 255, 0, 0),
    'orange': terminal.color_from_argb(255, 255, 127, 0),
    'light_cyan': terminal.color_from_argb(255, 115, 255, 255),
    'light_green': terminal.color_from_argb(255, 115, 255, 115),
    'light_pink': terminal.color_from_argb(255, 255, 115, 185),
    'light_yellow': terminal.color_from_argb(255, 255, 255, 115),
    'light_violet': terminal.color_from_argb(255, 185, 115, 255)
}


def get_game_variables():
    fighter_component = Fighter(hp=30, defense=2, power=5)
    inventory_component = Inventory(26)
    level_component = Level()

    player = Entity(0, 0, '@', colors['white'], 'Player', blocks=True,
                    render_order=RenderOrder.ACTOR, fighter=fighter_component, inventory=inventory_component, level=level_component)
    entities = [player]

    game_map = GameMap(map_width, map_height)
    game_map.make_map(max_rooms, room_min_size, room_max_size, map_width,
                      map_height, player, entities, max_monsters_per_room, max_items_per_room)

    message_log = MessageLog(message_x, message_width, message_height)

    game_state = States.PLAYERS_TURN

    return player, entities, game_map, message_log, game_state
