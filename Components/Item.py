class Item:
    """
    A representation of an item that can be picked up and carried.

    Args:
        use_function (function): A function to be called when the item is used.
        targeting (boolean): Whether the item requires targeting.
        targeting_message (~Game.Message): A message to display when using targeting.
    """

    def __init__(self, use_function=None, targeting=False, targeting_message=None, **kwargs):
        self.use_function = use_function
        self.targeting = targeting
        self.targeting_message = targeting_message
        self.function_kwargs = kwargs
