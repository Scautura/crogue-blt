from random import randint

import Constants
import tcod as libtcod
from Game import Message


class BasicMonster:
    """
    A basic monster AI that chases the player.
    """

    def take_turn(self, target, fov_map, game_map, entities):
        """
        Have the monster take its turn.

        Args:
            target (~Entity.Entity): An entity to chase.
            fov_map: A field of view map to work with.
            game_map (~MapObjects.GameMap): A game map to work with.
            entities (array): A list of entities to work around.

        Returns:
            array: An array of results, if the monster attacks the target, otherwise empty.
        """
        results = []

        monster = self.owner

        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):
            if monster.distance_to(target) >= 2:
                monster.move_astar(target, entities, game_map)

            elif target.fighter.hp > 0:
                attack_results = monster.fighter.attack(target)
                results.extend(attack_results)

        return results


class ConfusedMonster:
    """
    An AI for a monster that is confused.

    Args:
        previous_ai: The previous AI for the monster to return to.
        number_of_turns: The number of turns for the monster to be confused.
    """

    def __init__(self, previous_ai, number_of_turns=10):
        self.previous_ai = previous_ai
        self.number_of_turns = number_of_turns

    def take_turn(self, target, fov_map, game_map, entities):
        """
        Have the monster take its turn.

        Args:
            target (~Entity.Entity): An entity to chase.
            fov_map: A field of view map to work with.
            game_map (~MapObjects.GameMap): A game map to work with.
            entities (array): A list of entities to work around.

        Returns:
            array: An array of results, if the monster attacks the target, otherwise empty.
        """
        results = []

        if self.number_of_turns > 0:
            random_x = self.owner.x + randint(0, 2) - 1
            random_y = self.owner.y + randint(0, 2) - 1

            if random_x != self.owner.x and random_y != self.owner.y:
                self.owner.move_towards(random_x, random_y, game_map, entities)

            self.number_of_turns -= 1
        else:
            self.owner.ai = self.previous_ai
            results.append({'message': Message(
                'The {0} is no longer confused!'.format(self.owner.name), Constants.colors['red'])})

        return results
