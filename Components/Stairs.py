class Stairs:
    """
    A representation of stairs up or down.

    Args:
        floor (int): Which floor to go to when using these stairs.
    """

    def __init__(self, floor):
        self.floor = floor
