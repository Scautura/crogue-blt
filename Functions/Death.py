import tcod as libtcod
import Game
from Render import RenderOrder

def kill_player(player, colors):
    player.char = '%'
    player.color = colors['dark_red']

    return Game.Message('You died!', "red"), Game.States.PLAYER_DEAD


def kill_monster(monster, colors):
    death_message = Game.Message('{0} is dead!'.format(monster.name.capitalize()), "orange")

    monster.char = '%'
    monster.color = colors['dark_red']
    monster.blocks = False
    monster.fighter = None
    monster.ai = None
    monster.name = 'remains of ' + monster.name
    monster.render_order=RenderOrder.CORPSE

    return death_message
