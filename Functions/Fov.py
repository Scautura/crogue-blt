import tcod as libtcod


def InitializeFov(game_map):
    """
    Initializes the Field of View map and returns it.

    Args:
        game_map (~MapObjects.GameMap): A game map containing details of the area to deal with.

    Returns:
        ~tcod.map: A Field of View map object from tcod.
    """
    fov_map = libtcod.map_new(game_map.width, game_map.height)

    for y in range(game_map.height):
        for x in range(game_map.width):
            libtcod.map_set_properties(fov_map, x, y, not game_map.tiles[x][y].block_sight,
                                       not game_map.tiles[x][y].blocked)

    return fov_map


def RecomputeFov(fov_map, x, y, radius, light_walls=True, algorithm=0):
    """
    Recomputes the Field of View map with the given specifications.

    Args:
        fov_map (~tcod.map): A Field of View map.
        x (int): X co-ordinate specifying centre.
        y (int): Y co-ordinate specifying centre.
        radius (int): How wide to throw the Field of View.
        light_walls (boolean): Whether to light the walls or not.
        algorithm (int): Which algorithm to use to generate the Field of View.
    """
    libtcod.map_compute_fov(fov_map, x, y, radius, light_walls, algorithm)
