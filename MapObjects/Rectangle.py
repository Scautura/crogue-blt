class Rect:
    """
    A representation of a rectangle containing co-ordinates to specify the top left and bottom right.

    Args:
        x: Top left corner X co-ordinate.
        y: Top left corner Y co-ordinate.
        w: Width of the rectangle.
        h: Height of the rectangle.
    """

    def __init__(self, x, y, w, h):
        self.x1 = x
        self.y1 = y
        self.x2 = x + w
        self.y2 = y + h

    def center(self):
        """
        Find the centre point of a given rectangle.

        Returns:
            (int, int): X, Y co-ordinates of the centre point of the rectangle.
        """
        center_x = int((self.x1 + self.x2) / 2)
        center_y = int((self.y1 + self.y2) / 2)
        return (center_x, center_y)

    def intersect(self, other):
        """
        Find whether the rectangle intersects with a given different rectangle.

        Args:
            other (~MapObjects.Rectangle.Rect): The other rectangle to compare the the self rectangle.

        Returns:
            boolean: Whether the two rectangles intersect.
        """
        return (self.x1 <= other.x2 and self.x2 >= other.x1 and self.y1 <= other.y2 and self.y2 >= other.y1)
