from random import randint

import Constants
import tcod as libtcod
from Components.AI import BasicMonster
from Components.Fighter import Fighter
from Components.Item import Item
from Components.Stairs import Stairs
from Entity import Entity
from Functions.Item import cast_confuse, cast_fireball, cast_lightning, heal
from Game import Message
from MapObjects import Rectangle, Tile
from Render import RenderOrder


class GameMap:
    """
    A representation of a map in the form of a collection of :py:class:`~MapObjects.Tile.Tile` s.

    Args:
        width (int): How wide the :py:class:`~MapObjects.GameMap` is.
        height (int): How tall the :py:class:`~MapObjects.GameMap` is.
    """

    def __init__(self, width, height, dungeon_level=1):
        self.width = width
        self.height = height
        self.tiles = self.initialize_tiles()

        self.dungeon_level = dungeon_level

    def initialize_tiles(self):
        """
        Initialize map to be full of walls with the given width/height.

        Returns:
            array(~MapObjects.Tile.Tile): Return a 2-dimensional array of tiles.
        """
        tiles = [[Tile.Tile(True) for y in range(self.height)]
                 for x in range(self.width)]

        return tiles

    def make_map(self, max_rooms, room_min_size, room_max_size, map_width, map_height, player, entities, max_monsters_per_room, max_items_per_room):
        """
        Carve a map out with the given specifications.

        Args:
            max_rooms (int): Largest number of rooms to generate.
            room_min_size (int): Smallest width/height a room can be.
            room_max_size (int): Largest width/height a room can be.
            map_width (int): How wide the map is in tiles.
            map_height (int): How tall the map is in tiles.
            player (~Entity.Entity): An :py:class:`~Entity.Entity` representing the player.
            entities (array(~Entity.Entity)): An array of all entities.
            max_monsters_per_room (int): Maximum number of monsters in a room.
        """
        rooms = []
        num_rooms = 0

        center_of_last_room_x = None
        center_of_last_room_y = None

        for r in range(max_rooms):
            w = randint(room_min_size, room_max_size)
            h = randint(room_min_size, room_max_size)

            x = randint(0, map_width - w - 1)
            y = randint(0, map_height - h - 1)

            new_room = Rectangle.Rect(x, y, w, h)

            for other_room in rooms:
                if new_room.intersect(other_room):
                    break

            else:
                self.create_room(new_room)

                (new_x, new_y) = new_room.center()

                center_of_last_room_x = new_x
                center_of_last_room_y = new_y

                if num_rooms == 0:
                    player.x = new_x
                    player.y = new_y

                else:
                    (prev_x, prev_y) = rooms[num_rooms - 1].center()

                    if randint(0, 1) == 1:
                        self.create_h_tunnel(prev_x, new_x, prev_y)
                        self.create_v_tunnel(prev_y, new_y, new_x)

                    else:
                        self.create_v_tunnel(prev_y, new_y, prev_x)
                        self.create_h_tunnel(prev_x, new_x, new_y)

                self.place_entities(new_room, entities,
                                    max_monsters_per_room, max_items_per_room)

                rooms.append(new_room)
                num_rooms += 1

        stairs_component = Stairs(self.dungeon_level + 1)
        down_stairs = Entity(center_of_last_room_x, center_of_last_room_y, '>', Constants.colors[
                             "white"], 'Stairs', render_order=RenderOrder.STAIRS, stairs=stairs_component)
        entities.append(down_stairs)

    def create_room(self, room):
        """
        Dig out a room from the game map.

        Args:
            room (~MapObjects.Rectangle.Rect): A rectangle representing the room to be dug out.
        """
        for x in range(room.x1 + 1, room.x2):
            for y in range(room.y1 + 1, room.y2):
                self.tiles[x][y].blocked = False
                self.tiles[x][y].block_sight = False

    def create_h_tunnel(self, x1, x2, y):
        """
        Dig out a horizontal tunnel between two X co-ordinates.

        Args:
            x1 (int): Starting X co-ordinate.
            x2 (int): Finishing X co-ordinate.
            y: Y co-ordinate.
        """
        for x in range(min(x1, x2), max(x1, x2) + 1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False

    def create_v_tunnel(self, y1, y2, x):
        """
        Dig out a vertical tunnel between two Y co-ordinates.

        Args:
            y1 (int): Starting Y co-ordinate.
            y2 (int): Finishing Y co-ordinate.
            x: X co-ordinate.
        """
        for y in range(min(y1, y2), max(y1, y2) + 1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False

    def is_blocked(self, x, y):
        """
        Check whether a specific tile is blocked.

        Args:
            x (int): X co-ordinate to check.
            y (int): Y co-ordinate to check.

        Returns:
            boolean: Whether the tile is blocked.
        """
        if self.tiles[x][y].blocked:
            return True

        return False

    def place_entities(self, room, entities, max_monsters_per_room, max_items_per_room):
        """
        Place monsters in a given room.

        Args:
            room (~MapObjects.Rectangle.Rectangle): A rectangle representing a room.
            entities (array(~Entity.Entity)): An array of entities.
            max_monsters_per_room (int): Maximum number of monsters to place in a room.
            max_items_per_room (int): Maximum number of items to place in a room.
        """
        number_of_monsters = randint(0, max_monsters_per_room)
        number_of_items = randint(0, max_items_per_room)

        for i in range(number_of_monsters):
            x = randint(room.x1 + 1, room.x2 - 1)
            y = randint(room.y1 + 1, room.y2 - 1)

            if not any([entity for entity in entities if entity.x == x and entity.y == y]):
                if randint(0, 100) < 80:
                    fighter_component = Fighter(hp=10, defense=0, power=3, xp=35)
                    ai_component = BasicMonster()

                    monster = Entity(x, y, 'o', Constants.colors[
                                     'desaturated_green'], 'Orc', blocks=True, render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)
                else:
                    fighter_component = Fighter(hp=16, defense=1, power=4, xp=100)
                    ai_component = BasicMonster()

                    monster = Entity(x, y, 'T', Constants.colors[
                                     'darker_green'], 'Troll', blocks=True, render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)

                entities.append(monster)

        for i in range(number_of_items):
            x = randint(room.x1 + 1, room.x2 - 1)
            y = randint(room.y1 + 1, room.y2 - 1)

            if not any([entity for entity in entities if entity.x == x and entity.y == y]):
                item_chance = randint(0, 100)

                if item_chance < 70:
                    item_component = Item(use_function=heal, amount=4)
                    item = Entity(x, y, '!', Constants.colors[
                        'violet'], 'Healing Potion', render_order=RenderOrder.ITEM, item=item_component)
                elif item_chance < 80:
                    item_component = Item(use_function=cast_fireball, targeting=True, targeting_message=Message(
                        'Left-click a target tile for the fireball, or right-click to cancel.', Constants.colors['light_cyan']), damage=12, radius=3)
                    item = Entity(x, y, '#', Constants.colors[
                                  'red'], 'Fireball Scroll', render_order=RenderOrder.ITEM, item=item_component)
                elif item_chance < 90:
                    item_component = Item(use_function=cast_confuse, targeting=True, targeting_message=Message(
                        'Left-click an enemy to confuse it, or right-click to cancel.', Constants.colors['light_cyan']))
                    item = Entity(x, y, '#', Constants.colors[
                                  'light_pink'], 'Confusion Scroll', render_order=RenderOrder.ITEM, item=item_component)
                else:
                    item_component = Item(
                        use_function=cast_lightning, damage=20, maximum_range=5)
                    item = Entity(x, y, '#', Constants.colors[
                                  'yellow'], 'Lightning Scroll', render_order=RenderOrder.ITEM, item=item_component)

                entities.append(item)

    def next_floor(self, player, message_log):
        self.dungeon_level += 1
        entities = [player]

        self.tiles = self.initialize_tiles()
        self.make_map(Constants.max_rooms, Constants.room_min_size, Constants.room_max_size, Constants.map_width,
                      Constants.map_height, player, entities, Constants.max_monsters_per_room, Constants.max_items_per_room)

        player.fighter.heal(player.fighter.max_hp // 2)

        message_log.add_message(Message(
            'You take a moment to rest, and recover your strength.', Constants.colors['light_violet']))

        return entities
