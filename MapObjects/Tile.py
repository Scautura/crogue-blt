class Tile:
    """A tile in a map. It may or may not be blocked, and may or may not block sight.

    Args:
        blocked (bool): Whether the tile is blocked
        block_sight (bool): Thether the tile blocks sight
    """

    def __init__(self, blocked, block_sight=None):
        self.blocked = blocked

        if block_sight is None:
            block_sight = blocked

        self.block_sight = block_sight

        self.explored = False
