#!/usr/bin/python3
from bearlibterminal import terminal

import Constants
import Game
import Render
import tcod as libtcod
from Entity import get_blocking_entities_at_location
from Functions.Death import kill_monster, kill_player
from Functions.Fov import InitializeFov, RecomputeFov
from InputHandlers import handle_keys, handle_main_menu, handle_mouse


def play_game(player, entities, game_map, message_log, game_state):
    fov_recompute = True

    fov_map = InitializeFov(game_map)

    exit = False

    previous_game_state = game_state
    mouse = (0, 0)

    targeting_item = None

    while not exit:
        if fov_recompute:
            RecomputeFov(fov_map, player.x, player.y, Constants.fov_radius,
                         Constants.fov_light_walls, Constants.fov_algorithm)

        Render.render_all(entities, player, game_map, fov_map, fov_recompute, message_log, Constants.screen_width,
                          Constants.screen_height, Constants.bar_width, Constants.panel_height, Constants.panel_y, mouse, Constants.colors, game_state)

        fov_recompute = False

        terminal.refresh()
        Render.clear_all(entities)

        key = terminal.read()
        if key == terminal.TK_MOUSE_MOVE:
            mouse = (terminal.state(terminal.TK_MOUSE_X),
                     terminal.state(terminal.TK_MOUSE_Y))

        action = handle_keys(key, game_state)
        mouse_action = handle_mouse(mouse, key)

        move = action.get('move')
        wait = action.get('wait')
        exit = action.get('exit')
        pickup = action.get('pickup')
        show_inventory = action.get('show_inventory')
        drop_inventory = action.get('drop_inventory')
        inventory_index = action.get('inventory_index')
        take_stairs = action.get('take_stairs')
        level_up = action.get('level_up')
        show_character_screen = action.get('show_character_screen')

        left_click = mouse_action.get('left_click')
        right_click = mouse_action.get('right_click')

        player_turn_results = []

        if move and game_state == Game.States.PLAYERS_TURN:
            dx, dy = move
            destination_x = player.x + dx
            destination_y = player.y + dy

            if not game_map.is_blocked(destination_x, destination_y):
                target = get_blocking_entities_at_location(
                    entities, destination_x, destination_y)

                if target:
                    attack_results = player.fighter.attack(target)
                    player_turn_results.extend(attack_results)

                else:
                    player.move(dx, dy)

                    fov_recompute = True

                game_state = Game.States.ENEMY_TURN
        elif wait:
            game_state = Game.States.ENEMY_TURN

        elif pickup and game_state == Game.States.PLAYERS_TURN:
            for entity in entities:
                if entity.item and entity.x == player.x and entity.y == player.y:
                    pickup_results = player.inventory.add_item(entity)
                    player_turn_results.extend(pickup_results)

                    break
            else:
                message_log.add_message(Game.Message(
                    'There is nothing here to pick up.', Constants.colors['yellow']))

        if show_inventory:
            previous_game_state = game_state
            game_state = Game.States.SHOW_INVENTORY

        if drop_inventory:
            previous_game_state = game_state
            game_state = Game.States.DROP_INVENTORY

        if inventory_index is not None and previous_game_state != Game.States.PLAYER_DEAD and inventory_index < len(player.inventory.items):
            item = player.inventory.items[inventory_index]

            if game_state == Game.States.SHOW_INVENTORY:
                player_turn_results.extend(player.inventory.use(
                    item, entities=entities, fov_map=fov_map))
            elif game_state == Game.States.DROP_INVENTORY:
                player_turn_results.extend(player.inventory.drop_item(item))

        if take_stairs and game_state == Game.States.PLAYERS_TURN:
            for entity in entities:
                if entity.stairs and entity.x == player.x and entity.y == player.y:
                    entities = game_map.next_floor(player, message_log)
                    fov_map = InitializeFov(game_map)
                    fov_recompute = True
                    terminal.clear(0)

                    break
            else:
                message_log.add_message(Game.Message(
                    'There are no stairs here.', Constants.colors['yellow']))

        if level_up:
            if level_up == 'hp':
                player.fighter.max_hp += 20
                player.fighter.hp += 20
            if level_up == 'str':
                player.fighter.power += 1
            if level_up == 'def':
                player.fighter.defense += 1

            game_state = previous_game_state
            fov_recompute = True

        if show_character_screen:
            previous_game_state = game_state
            game_state = Game.States.CHARACTER_SCREEN

        if game_state == Game.States.TARGETING:
            if left_click:
                target_x, target_y = left_click

                item_use_results = player.inventory.use(
                    targeting_item, entities=entities, fov_map=fov_map, target_x=target_x, target_y=target_y)
                player_turn_results.extend(item_use_results)
            elif right_click:
                player_turn_results.append({'targeting_cancelled': True})

        if exit:
            if game_state in (Game.States.SHOW_INVENTORY, Game.States.DROP_INVENTORY, Game.States.CHARACTER_SCREEN):
                game_state = previous_game_state
                exit = False
                fov_recompute = True
            elif game_state == Game.States.TARGETING:
                player_turn_results.append({'targeting_cancelled': True})
            else:
                Game.File.save_game(
                    player, entities, game_map, message_log, game_state)
                return True

        for player_turn_result in player_turn_results:
            message = player_turn_result.get('message')
            dead_entity = player_turn_result.get('dead')
            item_added = player_turn_result.get('item_added')
            item_consumed = player_turn_result.get('consumed')
            item_dropped = player_turn_result.get('item_dropped')
            targeting = player_turn_result.get('targeting')
            targeting_cancelled = player_turn_result.get('targeting_cancelled')
            xp = player_turn_result.get('xp')

            if message:
                message_log.add_message(message)

            if targeting_cancelled:
                fov_recompute = True
                exit = False
                game_state = previous_game_state

                message_log.add_message(Game.Message('Targetting cancelled.'))

            if xp:
                leveled_up = player.level.add_xp(xp)
                message_log.add_message(Game.Message(
                    'You gain {0} experience points.'.format(xp)))

                if leveled_up:
                    message_log.add_message(Game.Message('Your battle skills grow stronger! You reached level {0}!'.format(
                        player.level.current_level, Constants.colors['yellow'])))
                    previous_game_state = game_state
                    game_state = Game.States.LEVEL_UP

            if dead_entity:
                if dead_entity == player:
                    message, game_state = kill_player(
                        dead_entity, Constants.colors)

                else:
                    message = kill_monster(dead_entity, Constants.colors)

                message_log.add_message(message)

            if item_added:
                entities.remove(item_added)

                game_state = Game.States.ENEMY_TURN

            if item_consumed:
                fov_recompute = True
                game_state = Game.States.ENEMY_TURN

            if targeting:
                fov_recompute = True
                previous_game_state = Game.States.PLAYERS_TURN
                game_state = Game.States.TARGETING

                targeting_item = targeting

                message_log.add_message(targeting_item.item.targeting_message)

            if item_dropped:
                fov_recompute = True
                entities.append(item_dropped)

                game_state = Game.States.ENEMY_TURN

        if game_state == Game.States.ENEMY_TURN:
            for entity in entities:
                if entity.ai:
                    enemy_turn_results = entity.ai.take_turn(
                        player, fov_map, game_map, entities)

                    for enemy_turn_result in enemy_turn_results:
                        message = enemy_turn_result.get('message')
                        dead_entity = enemy_turn_result.get('dead')

                        if message:
                            message_log.add_message(message)

                        if dead_entity:
                            if dead_entity == player:
                                message, game_state = kill_player(
                                    dead_entity, Constants.colors)

                            else:
                                message = kill_monster(
                                    dead_entity, Constants.colors)

                            message_log.add_message(message)

                    if game_state == Game.States.PLAYER_DEAD:
                        break
            else:
                game_state = Game.States.PLAYERS_TURN


def main():
    terminal.open()
    terminal.set('window: title=' + Constants.window_title + ', size=' + str(Constants.screen_width) +
                 'x' + str(Constants.screen_height) + '; input: filter=[keyboard, mouse];')
    terminal.setf('font: media/AnonymousPro-Regular.ttf, size=12')
    terminal.setf(
        '0xE100: media/menu_background1.png, resize=732x800, resize-mode=fit')
    terminal.refresh()

    player = None
    entities = []
    game_map = None
    message_log = None
    game_state = None
    exit = False
    mouse = (0, 0)

    show_main_menu = True
    show_load_error_message = False

    while not exit:
        if show_main_menu:
            terminal.clear()
            Render.main_menu(Constants.screen_width, Constants.screen_height)

            if show_load_error_message:
                Render.message_box('No save game to load', 50,
                                   Constants.screen_width, Constants.screen_height)

            terminal.refresh()

            key = terminal.read()
            if key == terminal.TK_MOUSE_MOVE:
                mouse = (terminal.state(terminal.TK_MOUSE_X),
                         terminal.state(terminal.TK_MOUSE_Y))

            action = handle_main_menu(key)

            new_game = action.get('new_game')
            load_saved_game = action.get('load_game')
            exit = action.get('exit')

            if show_load_error_message and (new_game or load_saved_game or exit):
                show_load_error_message = False
            elif new_game:
                player, entities, game_map, message_log, game_state = Constants.get_game_variables()
                game_state = Game.States.PLAYERS_TURN

                show_main_menu = False
            elif load_saved_game:
                try:
                    player, entities, game_map, message_log, game_state = Game.File.load_game()
                    show_main_menu = False
                except FileNotFoundError:
                    show_load_error_message = True
            elif exit:
                terminal.close()
                break

        else:
            terminal.clear()
            play_game(player, entities, game_map, message_log, game_state)

            show_main_menu = True


if __name__ == '__main__':
    main()
