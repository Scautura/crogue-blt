from bearlibterminal import terminal

import Game


def handle_keys(key, game_state):
    """
    Handle input and do what is necessary with the given input.

    key (bearlibterminal.key): A key code from BearLibTerminal.
    game_state (Enum): The current game state.
    """
    if game_state == Game.States.PLAYERS_TURN:
        return handle_player_turn_keys(key)
    elif game_state == Game.States.PLAYER_DEAD:
        return handle_player_dead_keys(key)
    elif game_state == Game.States.TARGETING:
        return handle_targeting_keys(key)
    elif game_state in (Game.States.SHOW_INVENTORY, Game.States.DROP_INVENTORY):
        return handle_inventory_keys(key)
    elif game_state == Game.States.LEVEL_UP:
        return handle_level_up_menu(key)
    elif game_state == Game.States.CHARACTER_SCREEN:
        return handle_character_screen(key)

    return {}


def handle_player_turn_keys(key):
    """
    Handle input and do what is necessary with the given input.

    key (bearlibterminal.key): A key code from BearLibTerminal.
    """
    if key == terminal.TK_UP or key == terminal.TK_KP_8 or key == terminal.TK_K:
        return {'move': (0, -1)}
    if key == terminal.TK_DOWN or key == terminal.TK_KP_2 or key == terminal.TK_J:
        return {'move': (0, 1)}
    if key == terminal.TK_LEFT or key == terminal.TK_KP_4 or key == terminal.TK_H:
        return {'move': (-1, 0)}
    if key == terminal.TK_RIGHT or key == terminal.TK_KP_6 or key == terminal.TK_L:
        return {'move': (1, 0)}
    if key == terminal.TK_KP_9 or key == terminal.TK_U:
        return {'move': (1, -1)}
    if key == terminal.TK_KP_7 or key == terminal.TK_Y:
        return {'move': (-1, -1)}
    if key == terminal.TK_KP_1 or key == terminal.TK_B:
        return {'move': (-1, 1)}
    if key == terminal.TK_KP_3 or key == terminal.TK_N:
        return {'move': (1, 1)}
    if key == terminal.TK_KP_5 or key == terminal.TK_Z:
        return {'wait': True}

    if key == terminal.TK_G:
        return {'pickup': True}

    if key == terminal.TK_I:
        return {'show_inventory': True}

    if key == terminal.TK_D:
        return {'drop_inventory': True}

    if chr(terminal.state(terminal.TK_CHAR)) == '>' or key == terminal.TK_ENTER:
        print("Stairs")
        return {'take_stairs': True}

    if key == terminal.TK_C:
        return {'show_character_screen': True}

    if key == terminal.TK_CLOSE or key == terminal.TK_ESCAPE:
        return {'exit': True}

    return {}


def handle_targeting_keys(key):
    if key == terminal.TK_ESCAPE:
        return {'exit': True}

    return {}


def handle_player_dead_keys(key):
    """
    Handle input and do what is necessary with the given input.

    key (bearlibterminal.key): A key code from BearLibTerminal.
    """
    if key == terminal.TK_I:
        return {'show_inventory': True}
    if key == terminal.TK_CLOSE or key == terminal.TK_ESCAPE:
        return {'exit': True}

    return {}


def handle_inventory_keys(key):
    """
    Handle input and do what is necessary with the given input.

    key (bearlibterminal.key): A key code from BearLibTerminal.
    """
    if key == terminal.TK_CLOSE or key == terminal.TK_ESCAPE:
        return {'exit': True}

    index = key - terminal.TK_A

    if index >= 0:
        return {'inventory_index': index}

    return {}


def handle_main_menu(key):
    if key == terminal.TK_A:
        return {'new_game': True}
    elif key == terminal.TK_B:
        return {'load_game': True}
    elif key == terminal.TK_C or key == terminal.TK_ESCAPE:
        return {'exit': True}

    return {}


def handle_level_up_menu(key):
    if key:
        if key == terminal.TK_A:
            return {'level_up': 'hp'}
        if key == terminal.TK_B:
            return {'level_up': 'str'}
        if key == terminal.TK_C:
            return {'level_up': 'def'}

    return {}


def handle_character_screen(key):
    if key == terminal.TK_ESCAPE:
        return {'exit': True}

    return {}


def handle_mouse(mouse, key):
    (x, y) = (mouse[0], mouse[1])

    if key == terminal.TK_MOUSE_LEFT:
        return {'left_click': (x, y)}
    elif key == terminal.TK_MOUSE_RIGHT:
        return {'right_click': (x, y)}

    return {}
