import textwrap

from bearlibterminal import terminal

import Constants
import Game
import tcod as libtcod
from enum import Enum, auto


class RenderOrder(Enum):
    STAIRS = auto()
    CORPSE = auto()
    ITEM = auto()
    ACTOR = auto()


def get_names_under_mouse(mouse, entities, fov_map):
    (x, y) = (mouse[0], mouse[1])

    names = [entity.name for entity in entities
             if entity.x == x and entity.y == y and libtcod.map_is_in_fov(fov_map, entity.x, entity.y)]
    names = ', '.join(names)

    return names.capitalize()


def render_bar(x, y, total_width, name, value, maximum, bar_color, back_color):
    bar_width = int(float(value) / maximum * total_width)

    terminal.bkcolor(back_color)
    terminal.clear_area(x, y + Constants.panel_y, total_width, 1)

    terminal.bkcolor(bar_color)
    if bar_width > 0:
        terminal.clear_area(x, y + Constants.panel_y, bar_width, 1)

    terminal.bkcolor(terminal.color_from_name("transparent"))
    terminal.color(terminal.color_from_argb(255, 255, 255, 255))
    output = '{0}: {1}/{2}'.format(name, value, maximum)
    offset, temp = terminal.measure(output)
    terminal.printf(int(x + total_width / 2) - offset // 2,
                    y + Constants.panel_y, output)


def render_all(entities, player, game_map, fov_map, fov_recompute, message_log, screen_width, screen_height, bar_width, panel_height, panel_y, mouse, colors, game_state, layer=0):
    """
    Render everything to the main game screen.

    Args:
        entities (array(~Entity.Entity)): An array of Entities to be rendered to screen.
        game_map (~MapObjects.GameMap): An object containing a representation of the map to be rendered.
        screen_width (int): Screen width in characters.
        screen_height (int) Screen height in characters.
        colors (array(bearlibterminal.color)): An array of colors to be used when rendering.
        game_state (Enum): The current state of the game.
        layer (int): The bearlibterminal layer to render to (all layers other than 0 do not render a background).
    """
    terminal.layer(layer)

    if fov_recompute:
        terminal.clear_area(0, 0, screen_width, screen_height)
        for y in range(game_map.height):
            for x in range(game_map.width):
                visible = libtcod.map_is_in_fov(fov_map, x, y)
                wall = game_map.tiles[x][y].block_sight
                if visible:
                    if wall:
                        terminal.printf(
                            x, y, '[bkcolor=' + str(colors.get('light_wall')) + '] ')
                    else:
                        terminal.printf(
                            x, y, '[bkcolor=' + str(colors.get('light_ground')) + '] ')
                    game_map.tiles[x][y].explored = True
                elif game_map.tiles[x][y].explored:
                    if wall:
                        terminal.printf(
                            x, y, '[bkcolor=' + str(colors.get('dark_wall')) + '] ')
                    else:
                        terminal.printf(
                            x, y, '[bkcolor=' + str(colors.get('dark_ground')) + '] ')

    entities_in_render_order = sorted(
        entities, key=lambda x: x.render_order.value)

    for entity in entities_in_render_order:
        draw_entity(entity, fov_map, game_map, layer)

    terminal.bkcolor("black")
    terminal.clear_area(0, panel_y, screen_width, panel_height)

    y = 1
    for message in message_log.messages:
        terminal.color(message.color)
        terminal.printf(message_log.x, Constants.panel_y + y, message.text)
        y += 1

    render_bar(1, 1, bar_width, 'HP', player.fighter.hp,
               player.fighter.max_hp, colors["light_red"], colors["darker_red"])

    terminal.printf(
        1, panel_y + 3, 'Dungeon level: {0}'.format(game_map.dungeon_level))

    terminal.color(terminal.color_from_argb(255, 159, 159, 159))
    terminal.printf(1, panel_y, get_names_under_mouse(
        mouse, entities, fov_map))

    if game_state in (Game.States.SHOW_INVENTORY, Game.States.DROP_INVENTORY):
        if game_state == Game.States.SHOW_INVENTORY:
            inventory_title = 'Press the key next to an item to use it, or Esc to cancel\n'
        else:
            inventory_title = 'Press the key next to an item to drop it, or Esc to cancel\n'

        inventory_menu(inventory_title,
                       player.inventory, 50, screen_width, screen_height)

    elif game_state == Game.States.LEVEL_UP:
        level_up_menu('Level up! Choose a stat to raise:',
                      player, 40, screen_width, screen_height)

    elif game_state == Game.States.CHARACTER_SCREEN:
        character_screen(player, 30, 10, screen_width, screen_height)


def clear_all(entities, layer=0):
    """
    Clear all entities from a specified layer.

    Args:
        entities (array(~Entity.Entity)): An array of entities to clear from a layer.
        layer (int): The bearlibterminal layer to clear entities from.
    """
    for entity in entities:
        clear_entity(entity, layer)


def draw_entity(entity, fov_map, game_map, layer=0):
    """
    Draw an entity to a specified layer.

    Args:
        entity (~Entity.Entity): An entity to draw to the screen.
        layer (int): A bearlibterminal layer to draw to.
    """
    terminal.layer(layer)
    if libtcod.map_is_in_fov(fov_map, entity.x, entity.y) or (entity.stairs and game_map.tiles[entity.x][entity.y].explored):
        terminal.printf(entity.x, entity.y,
                        '[color=' + str(entity.color) + ']' + entity.char)


def clear_entity(entity, layer=0):
    """Clear a specified entity from the specified layer.

    Args:
        entity (~Entity.Entity): An entity to clear from the layer.
        layer (int): The bearlibterminal layer to clear the entity from.
    """
    terminal.layer(layer)
    terminal.printf(entity.x, entity.y, '[color=' + str(entity.color) + '] ')


def menu(header, options, width, screen_width, screen_height, layer=0):
    if len(options) > 26:
        raise ValueError('Cannot have a menu with more than 26 options.')

    header_height = terminal.measure(header, width)
    height = len(options) + header_height[1]

    x = int(screen_width / 2 - width / 2)
    y = int(screen_height / 2 - height / 2)

    # terminal.composition(True)
    terminal.bkcolor(terminal.color_from_argb(100, 0, 0, 0))
    for text in textwrap.wrap(header, width):
        terminal.printf(x, y, text.ljust(width, ' '))
        y += 1
    y = int(screen_height / 2 - height / 2)
    terminal.printf(x, y + header_height[1] - 1, ' ' * width)

    i = header_height[1]
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ')' + option_text
        terminal.printf(x, y + i, text.ljust(width, ' '))
        i += 1
        letter_index += 1

    terminal.bkcolor(terminal.color_from_name("transparent"))


def inventory_menu(header, inventory, inventory_width, screen_width, screen_height, layer=0):
    if len(inventory.items) == 0:
        options = ['Inventory is empty']
    else:
        options = [item.name for item in inventory.items]

    menu(header, options, inventory_width, screen_width, screen_height, layer)


def main_menu(screen_width, screen_height, layer=0):
    terminal.color(Constants.colors['light_yellow'])

    terminal.printf(0, 0, '[U+E100]')
    terminal.printf(screen_width // 2 - 13, screen_height //
                    2 - 4, 'TOMBS OF THE ANCIENT KINGS')
    terminal.printf(screen_width // 2 - 10, screen_height -
                    2, 'By (Your name here)')

    menu('', ['Play a new game', 'Continue last game', 'Quit'],
         24, screen_width, screen_height, layer)


def level_up_menu(header, player, menu_width, screen_width, screen_height, layer=0):
    options = ['Constitution (+20 HP, from {0})'.format(player.fighter.max_hp),
               'Strength (+1 attack, from {0})'.format(player.fighter.power),
               'Agility (+1 defense, from {0})'.format(player.fighter.defense)]

    menu(header, options, menu_width, screen_width, screen_height, layer)


def character_screen(player, character_screen_width, character_screen_height, screen_width, screen_height):
    x = screen_width // 2 - character_screen_width // 2
    y = screen_height // 2 - character_screen_height // 2

    terminal.bkcolor(terminal.color_from_name("black"))
    terminal.color(terminal.color_from_name("white"))

    terminal.printf(
        x, y + 1, 'Character Information'.ljust(character_screen_width, ' '))
    terminal.printf(
        x, y + 2, ('Level: {0}'.format(player.level.current_level)).ljust(character_screen_width, ' '))
    terminal.printf(x, y + 3, ('Experience: {0}'.format(
        player.level.current_xp)).ljust(character_screen_width, ' '))
    terminal.printf(x, y + 4, ('Experience to level: {0}'.format(
        player.level.experience_to_next_level)).ljust(character_screen_width, ' '))
    terminal.printf(x, y + 5, ' ' * character_screen_width)
    terminal.printf(x, y + 6, ('Maximum HP: {0}'.format(
        player.fighter.max_hp)).ljust(character_screen_width, ' '))
    terminal.printf(
        x, y + 7, ('Attack: {0}'.format(player.fighter.power)).ljust(character_screen_width, ' '))
    terminal.printf(
        x, y + 8, ('Defense: {0}'.format(player.fighter.defense)).ljust(character_screen_width, ' '))

    terminal.bkcolor(terminal.color_from_name("transparent"))

def message_box(header, width, screen_width, screen_height, layer=0):
    menu(header, [], width, screen_width, screen_height, layer)
