import math

import tcod as libtcod
from Render import RenderOrder


class Entity:
    """A generic object to represent players, enemies, items, etc.

    Args:
        x (int): X co-ordinate of object
        y (int): Y co-ordinate of object
        char (char): Character to print for object
        color (~bearlibterminal.color): Color to print for object
        name (str): A name for the entity.
        blocks (boolean): Whether the entity blocks movement.
        fighter (class): A class containing a fighter structure.
        ai (class): A class containing an AI structure.
    """

    def __init__(self, x, y, char, color, name, blocks=False, render_order=RenderOrder.CORPSE, fighter=None, ai=None, item=None, inventory=None, stairs=None, level=None):
        self.x = x
        self.y = y
        self.char = char
        self.color = color
        self.name = name
        self.blocks = blocks
        self.render_order = render_order
        self.fighter = fighter
        self.ai = ai
        self.item = item
        self.inventory = inventory
        self.stairs = stairs
        self.level = level

        if self.fighter:
            self.fighter.owner = self

        if self.ai:
            self.ai.owner = self

        if self.item:
            self.item.owner = self

        if self.inventory:
            self.inventory.owner = self

        if self.stairs:
            self.stairs.owner = self

        if self.level:
            self.level.owner = self

    def move(self, dx, dy):
        """Move an Entity by a certain amount.

        Args:
            dx (int): Amount to move Entity along the X axis (can be negative)
            dy (int): Amount to move Entity along the Y axis (can be negative)
        """
        self.x += dx
        self.y += dy

    def move_towards(self, target_x, target_y, game_map, entities):
        """
        Move towards a given position.

        Args:
            target_x (int): Target X co-ordinate.
            target_y (int): Target Y co-ordinate.
            game_map (~MapObjects.GameMap): A game map to use for movement.
            entities (array): An array of entities to check for blocking.
        """
        dx = target_x - self.x
        dy = target_y - self.y
        distance = math.sqrt(dx**2 + dy**2)

        dx = int(round(dx / distance))
        dy = int(round(dy / distance))

        if not(game_map.is_blocked(self.x + dx, self.y + dy) or get_blocking_entities_at_location(entities, self.x + dx, self.y + dy)):
            self.move(dx, dy)

    def move_astar(self, target, entities, game_map):
        """
        Use A* algorithm for pathfinding to a target entity.

        Args:
            target (~Entity.Entity): An entity to move towards.
            entities (array): An arroy of entities to check for blocking.
            game_map (~MapObjects.GameMap): A game map to use for movement.
        """
        fov = libtcod.map_new(game_map.width, game_map.height)

        for y1 in range(game_map.height):
            for x1 in range(game_map.width):
                libtcod.map_set_properties(fov, x1, y1, not game_map.tiles[x1][
                                           y1].block_sight, not game_map.tiles[x1][y1].blocked)

        for entity in entities:
            if entity.blocks and entity != self and entity != target:
                libtcod.map_set_properties(
                    fov, entity.x, entity.y, True, False)

        my_path = libtcod.path_new_using_map(fov, 1.41)

        libtcod.path_compute(my_path, self.x, self.y, target.x, target.y)

        if not libtcod.path_is_empty(my_path) and libtcod.path_size(my_path) < 25:
            x, y = libtcod.path_walk(my_path, True)
            if x or y:
                self.x = x
                self.y = y
        else:
            self.move_towards(target.x, target.y, game_map, entities)

        libtcod.path_delete(my_path)

    def distance(self, x, y):
        """
        Return a distance to a position.

        Args:
            x (int): X co-ordinate to measure.
            y (int): Y co-ordinate to measure.

        Returns:
            (float): The distance to the position.
        """
        return math.sqrt((x - self.x)**2 + (y - self.y)**2)

    def distance_to(self, other):
        """
        Return a distance to an entity.

        Args:
            other (~Entity.Entity): The entity to measure the distance to.

        Returns:
            (float): The distance to the entity.
        """
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx**2 + dy**2)


def get_blocking_entities_at_location(entities, destination_x, destination_y):
    """
    Whether entities at a given position block movement into that position.

    Args:
        entities (array): A list of entities to check.
        destination_x (int): The X co-ordinate to check.
        destination_y (int): The Y co-ordinate to check.

    Returns:
        (~Entity.Entity): An entity if it blocks movement, or None.
    """
    for entity in entities:
        if entity.blocks and entity.x == destination_x and entity.y == destination_y:
            return entity

    return None
