import textwrap
import Game.File

from bearlibterminal import terminal

from enum import Enum, auto


class States(Enum):
    """
    States the game can be in.
    """
    PLAYERS_TURN = auto()
    ENEMY_TURN = auto()
    PLAYER_DEAD = auto()
    SHOW_INVENTORY = auto()
    DROP_INVENTORY = auto()
    TARGETING = auto()
    LEVEL_UP = auto()
    CHARACTER_SCREEN = auto()


class Message:
    """
    A message to be displayed on screen in a given color.

    Args:
        text (string): The text to be displayed.
        color (string): The color to display the message. Can be in the form of a BearLibTerminal ARGB color, or a text string.
    """

    def __init__(self, text, color="white"):
        self.text = text
        self.color = color


class MessageLog:
    """
    A log of messages to display on screen.

    Args:
        x (int): X position to display the message log.
        width (int): How wide (in characters) to allow the log to be.
        height (int): How tall (in characters) to allow the log to be.
    """

    def __init__(self, x, width, height):
        self.messages = []
        self.x = x
        self.width = width
        self.height = height

    def add_message(self, message):
        """
        Add a message to the log and do any required text wrapping.

        Args:
            message (~Game.Message): A message to be added to the log.
        """
        new_msg_lines = textwrap.wrap(message.text, self.width)

        for line in new_msg_lines:
            if len(self.messages) == self.height:
                del self.messages[0]

            self.messages.append(Message(line, message.color))
